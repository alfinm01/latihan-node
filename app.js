const app = require('express')()
const port = 3000;

const pgp = require('pg-promise')()
const db = pgp('postgres://lasti@103.122.5.98:51751/lastidb')

function getPegawai() {
	return new Promise((resolve, reject) => {
		db.any('SELECT * FROM pegawai')
			.then(data => {
				resolve(data);
			})
			.catch(err => {
				reject(err);
			})
	})
}

function postPegawai() {
	return new Promise((resolve, reject) => {
		db.any(
				`INSERT INTO pegawai(id, nama, alamat, kontak, email, jabatan, gaji, status) 
				VALUES($1, $2, $3, $4, $5, $6, $7, $8)`,
				[99, 'fullbeastmode', 'skribbl.io', 'anu', 'anuu', 'anuuu', null, 2]
			)
			.then(data => {
				resolve(data);
			})
			.catch(err => {
				reject(err);
			})
	})
}

/*app.get('/pegawai', function(req, res) {
	getPegawai()
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(reject => {
			console.log(err);
			res.send(err);
		})
})*/

app.get('/pegawai', async function(req, res) {
	try {
		const pegawai = await getPegawai();
		res.send(pegawai);
	}
	catch(err) {
		console.log(err);
		res.send(err);
	}
})

app.get('/pegawaiTapiPost', async function(req, res) {
	try {
		await postPegawai();
		res.send(await getPegawai());
	}
	catch(err) {
		console.log(err);
		res.send(err);
	}
})

console.info(`Server listening on port` + port); 
app.listen(port);